
import Vue from 'vue'
import App from './App'
// import router from './router/router'
import store from './store/store'
// import { sync } from 'vuex-router-sync'
// sync(store, router)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  // router,
  store,
  components: { App },
  template: '<App/>',
  created() {},
  mounted() {

    // #TODO
    this.$store.dispatch('Master/init')
    this.$store.dispatch('Sends/init')
    this.$store.dispatch('Clock/init')
    this.$store.dispatch('Sequencers/init')

  },
})
