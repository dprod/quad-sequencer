
const Fader = ({ minValue = 0.00, maxValue = 1.00, value = 0.5, orientation = 'VERTICAL' }) => ({

	fader: null,
	faderBar: null,

	orientation: orientation, // VERTICAL, HORIZONTAL

	gestureActive: false,

	minValue: minValue,
	maxValue: maxValue,

	value: value,

	callback: () => {},

	create({ parentEl = document.body, styles = false }) {

		this.fader = document.createElement('div')
		this.fader.classList.add('fader')

		this.faderBar = document.createElement('div')
		this.faderBar.classList.add('fader-bar')

		this.fader.appendChild(this.faderBar)
		parentEl.appendChild(this.fader)

		this.addEvents()

		if(styles)
			this.createStyles(styles)

		this.updateValueBar()

		return this

	},

	addEvents() {

		this.fader.addEventListener('mousedown', (event) => {
			this.gestureStart(event)
		})
		this.fader.addEventListener('touchstart', (event) => {
			this.gestureStart(event)
		})

		document.body.addEventListener('mousemove', (event) => {
			this.gestureMove(event)
		})
		document.body.addEventListener('touchmove', (event) => {
			this.gestureMove(event)
		})


		document.body.addEventListener('mouseup', (event) => {
			this.gestureStop()
		})
		document.body.addEventListener('touchend', (event) => {
			this.gestureStop()
		})

	},

	gestureStart(event) {
		
		this.gestureActive = true
		this.value = this.getValue(event)	
		this.update()

	},

	gestureMove(event) {

		if(this.gestureActive) {
			this.value = this.getValue(event)	
			this.update()
		}

	},

	gestureStop() {
		this.gestureActive = false
		this.update()
	},

	update() {
		if(this.gestureActive)
			this.onUpdate()
		this.updateStyles()
	},

	onUpdate(cb) {

		if(cb)
			this.callback = cb

		return this.callback(this.value)

	},

	getValue(event) {

		if(!event)
			return

		// #TODO!!!
		// normalize value to minValue and maxValue!!!!!!

		let rect = this.fader.getBoundingClientRect()

		let max = 0
		if(this.orientation === 'VERTICAL')
			max = this.fader.offsetHeight
		else if (this.orientation === 'HORIZONTAL')
			max = this.fader.offsetWidth
		let min = 0

		// #TODO?
		let value = 0
		if(this.orientation === 'VERTICAL')
			value = max - (event.pageY || (event.touches && event.touches[0].pageY)) + rect.top
		else if (this.orientation === 'HORIZONTAL')
			value = (event.pageX || (event.touches && event.touches[0].pageX)) - rect.left

		if(!value || isNaN(value))
			value = 0

		let normalizedValue = this.normalizeValue({ value, min, max, to: 1 })

		return normalizedValue

	},

	normalizeValue({ value = 0, min = 0, max = 500, to = 1 }) {

		// Keep value in range
		value = (value > max) ? max : value
		value = (value < min) ? min : value

		// Normalize value
		let normalized = (value - min) / (max - min)
		normalized = normalized * to

		return normalized

	},

	updateStyles() {
		if(this.gestureActive) {
			document.body.classList.add('gesture-active')
			document.body.style.userSelect = 'none'
			document.body.style.cursor = 'ns-resize'
			document.body.style.overflow = 'hidden'

		} else {
			document.body.classList.remove('gesture-active')
			document.body.style.userSelect = 'inherit'
			document.body.style.cursor = 'inherit'
			document.body.style.overflow = 'inherit'
		}
		this.updateValueBar()
	},

	updateValueBar() {

		if(this.orientation === 'VERTICAL') {
			this.faderBar.style.height = `${this.value*100}%`
		}
		else if (this.orientation === 'HORIZONTAL') {
			this.faderBar.style.width = `${this.value*100}%`
		}


	},

	createStyles({ width = 40, height = 100 }) {

		this.fader.style.left = '100px'
		this.fader.style.zIndex = '100000000'


		this.fader.style.display = 'inline-flex'
		this.fader.style.background = 'cyan'
		this.fader.style.position = 'relative'
		this.fader.style.margin = '0 5px'
		this.fader.style.width = `${width}`
		this.fader.style.height = `${height}`
		this.fader.style.userSelect = 'none'

		if(this.orientation === 'VERTICAL') {
			this.fader.style.cursor = 'ns-resize'
		}
		else if (this.orientation === 'HORIZONTAL') {
			this.fader.style.cursor = 'ew-resize'
		}

		this.faderBar.style.background = 'pink'
		this.faderBar.style.position = 'absolute'
		this.faderBar.style.bottom = '0'
		this.faderBar.style.left = '0'

		if(this.orientation === 'VERTICAL') {
			this.faderBar.style.width = '100%'
			this.faderBar.style.height = '10%'
			this.faderBar.style.cursor = 'ns-resize'
		}
		else if (this.orientation === 'HORIZONTAL') {
			this.faderBar.style.width = '10%'
			this.faderBar.style.height = '100%'
			this.faderBar.style.cursor = 'ew-resize'
		}

		// gestureActive = 
		// cursor: pointer;
		// cursor: hand;
		// cursor: -webkit-grab;
		// cursor: grab;

		return this

	},

	getElement() {
		return this.fader
	}
	
})

export default Fader
