
const namespaced = true

const state = {

  init: false,

  context: null,

  masterGain: null,

  disabled: false,

}

const mutations = {

	INIT (state) {
        
    state.context = new window.AudioContext()
    state.masterGain = state.context.createGain()
    state.masterGain.gain.setValueAtTime(0.7, state.context.currentTime)

    state.masterGain.connect(state.context.destination)

		state.init = true

  },
  
  DISABLE_AUDIO_ENGINE (state, init) {

    state.disabled = true
      
  },

  
  START_CLOCK (state) {

      state.isPlaying = true

  },
  
  STOP_CLOCK (state) {

      state.isPlaying = true

  },


}

const actions = {

	init ({ commit, dispatch }) {
        
    try {
      window.AudioContext = window.AudioContext || window.webkitAudioContext
    } catch (e) {
      window.alert('Your browser has no Web Audio API support...')
      commit('DISABLE_AUDIO_ENGINE')
    }

		commit('INIT')

  },

}

const getters = {

  context: (state, rootState) => {

    return state.context

  },

  masterGain: (state, rootState) => {

    return state.masterGain

  },

  isDisabled: (state) => state.disabled,

}

export default {
  namespaced,
	state,
	mutations,
	actions,
	getters
}
