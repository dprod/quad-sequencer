
const namespaced = true

const state = {

  init: false,
  
  patterns: [
    {
      name: 'default',
      steps: [
        { number: 1, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 2, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 3, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 4, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 5, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 6, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 7, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 8, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 9, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 10, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 11, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 12, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 13, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 14, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 15, enabled: true, pitch: 0, filterCutoff: 2000, },
        { number: 16, enabled: true, pitch: 0, filterCutoff: 2000, },
      ]
    },
    {
      name: 'default-empty',
      steps: [
        { number: 1, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 2, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 3, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 4, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 5, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 6, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 7, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 8, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 9, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 10, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 11, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 12, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 13, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 14, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 15, enabled: false, pitch: 0, filterCutoff: 2000, },
        { number: 16, enabled: false, pitch: 0, filterCutoff: 2000, },
      ]
    },
    {
      name: 'arpeggio-up-3',
      steps: [
        { number: 1, enabled: true, pitch: -12, filterCutoff: 1600, },
        { number: 2, enabled: true, pitch: 0, filterCutoff: 1700, },
        { number: 3, enabled: true, pitch: 12, filterCutoff: 1800, },
        { number: 4, enabled: true, pitch: -12, filterCutoff: 2000, },
        { number: 5, enabled: true, pitch: 0, filterCutoff: 2100, },
        { number: 6, enabled: true, pitch: 12, filterCutoff: 2200, },
        { number: 7, enabled: true, pitch: -12, filterCutoff: 2300, },
        { number: 8, enabled: true, pitch: 0, filterCutoff: 2400, },
        { number: 9, enabled: true, pitch: 12, filterCutoff: 2600, },
        { number: 10, enabled: true, pitch: -12, filterCutoff: 2800, },
        { number: 11, enabled: true, pitch: 0, filterCutoff: 3000, },
        { number: 12, enabled: true, pitch: 12, filterCutoff: 3200, },
        { number: 13, enabled: true, pitch: -12, filterCutoff: 3400, },
        { number: 14, enabled: true, pitch: 0, filterCutoff: 3600, },
        { number: 15, enabled: true, pitch: 12, filterCutoff: 3800, },
        { number: 16, enabled: true, pitch: -12, filterCutoff: 4000, },
      ]
    },
    {
      name: 'bassline-1',
      steps: [
        { number: 1, enabled: true, pitch: -12, filterCutoff: 600, },
        { number: 2, enabled: true, pitch: 0, filterCutoff: 700, },
        { number: 3, enabled: true, pitch: -12, filterCutoff: 620, },
        { number: 4, enabled: true, pitch: 0, filterCutoff: 720, },
        { number: 5, enabled: true, pitch: -12, filterCutoff: 640, },
        { number: 6, enabled: true, pitch: 0, filterCutoff: 740, },
        { number: 7, enabled: true, pitch: -12, filterCutoff: 660, },
        { number: 8, enabled: true, pitch: 0, filterCutoff: 760, },
        { number: 9, enabled: true, pitch: -12, filterCutoff: 680, },
        { number: 10, enabled: true, pitch: 0, filterCutoff: 780, },
        { number: 11, enabled: true, pitch: -12, filterCutoff: 700, },
        { number: 12, enabled: true, pitch: 0, filterCutoff: 800, },
        { number: 13, enabled: true, pitch: -9, filterCutoff: 2900, },
        { number: 14, enabled: true, pitch: 3, filterCutoff: 2820, },
        { number: 15, enabled: true, pitch: -7, filterCutoff: 2920, },
        { number: 16, enabled: true, pitch: 5, filterCutoff: 2840, },
      ]
    },
  ]

}

const mutations = {

	INIT (state) {

    state.init = true

  },
  
  ADD_PATTERN (state, pattern) {

    state.patterns.push(pattern)

  }

}

const actions = {

	init ({ commit, dispatch }) {

		commit('INIT')

  },
    
  addPattern ({ commit, dispatch }, pattern) {

    commit('ADD_PATTERN', pattern)

  },

  loadPatternByName ({ commit, dispatch }, patternName) {

    let pattern = state.patterns.find((p) => p.name === patternName)

    // return pattern
    return JSON.parse(JSON.stringify(pattern))

  }

}

const getters = {

  patterns: (state) => {

    return state.patterns

  },

}

export default {
  namespaced,
	state,
	mutations,
	actions,
	getters
}
