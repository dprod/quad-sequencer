
const namespaced = true

const state = {
    
  baseVolume: 0.5,
  basePitch: 0,
  baseFilterCutoff: 0,

}

const mutations = {

}

const actions = {

  playVoice({ commit, dispatch }, { waveform, volume, pitch, filterCutoff, delaySendLevel, reverbSendLevel }) {

    // #TODO: Split this quite big and nice function we have here...

    // ================================
    // CREATE VOICE 
    // ================================

    /*

      OSCILLATOR      ->
        FILTER        ->
          DISTORTION  ->
            GAIN      ->

    */

    // Shortcut for context
    const context = this.getters['Master/context']

    // Gain
    const synthGain = context.createGain()
    synthGain.gain.setValueAtTime(0, context.currentTime)
    
    // Oscillator        
    const oscillator = context.createOscillator()
    oscillator.type = waveform

    // Filter
    const filter = context.createBiquadFilter()
    filter.type = 'lowpass'
    filter.frequency.setValueAtTime(1000, context.currentTime)
    filter.Q.setValueAtTime(10, context.currentTime)
    
    // Distortion / WaveShaper
    const distortion = context.createWaveShaper()
    distortion.curve = makeDistortionCurve(0)
    distortion.oversample = '4x'

    // Connect 
    oscillator.connect(filter)
    filter.connect(distortion)
    distortion.connect(synthGain)
    synthGain.connect(this.getters['Master/masterGain'])
    
    // Sends
    let delaySendGain = context.createGain()
    delaySendGain.gain.setValueAtTime(delaySendLevel, context.currentTime)
    synthGain.connect(delaySendGain)
    let delay = this.getters['Sends/getSendEffectByName']('delay')
    delaySendGain.connect(delay.effect)

    let reverbSendGain = context.createGain()
    reverbSendGain.gain.setValueAtTime(reverbSendLevel, context.currentTime)
    synthGain.connect(reverbSendGain)
    let reverb = this.getters['Sends/getSendEffectByName']('reverb')
    reverbSendGain.connect(reverb.effect)

    // Start
    oscillator.start(context.currentTime)       

    // ================================
    // PLAY VOICE 
    // ================================

    // Set oscillator frequency
    oscillator.frequency.setValueAtTime(getFrequencyByNoteNumber(pitch), context.currentTime)

    // Filter Attack & Decay
    filter.frequency.setValueAtTime(filterCutoff, context.currentTime)
    filter.frequency.exponentialRampToValueAtTime(0.01, context.currentTime + 1.0)

    // Gain Attack Decay Gate/Sustain(?) Release
    const gainAttackTime = 0.01
    const gainDecayTime = 2.00
    const gainReleaseTime = 2.00

    // Gate time / length?
    const gateTime = 0.00

    // Gate On
    synthGain.gain.setValueAtTime(0.00000001, context.currentTime)
    
    // Attack _/
    synthGain.gain.exponentialRampToValueAtTime(volume, context.currentTime + gainAttackTime)

    // Gate length / Sustain #TODO
    synthGain.gain.exponentialRampToValueAtTime(volume, context.currentTime + gainAttackTime + gateTime)
    
    // Decay
    // synthGain.gain.exponentialRampToValueAtTime(0.00000001, context.currentTime + gainAttackTime + gainDecayTime)

    // Release
    synthGain.gain.exponentialRampToValueAtTime(0.00000001, context.currentTime + gainAttackTime + gateTime + gainReleaseTime)
  
    oscillator.stop(context.currentTime + gainAttackTime + gateTime + gainReleaseTime)

    // Disconnect all components on stop
    oscillator.onended = () => {
      
      delaySendGain.disconnect()
      reverbSendGain.disconnect()

      oscillator.disconnect()
      filter.disconnect()
      distortion.disconnect()
      synthGain.disconnect() 

    }

  },

}

function getFrequencyByNoteNumber(noteNumber) {
  return 440 * Math.pow(2, (noteNumber - 59) / 12)
}

function makeDistortionCurve(amount) {
  var k = typeof amount === 'number' ? amount : 50,
    n_samples = 44100,
    curve = new Float32Array(n_samples),
    deg = Math.PI / 180,
    i = 0,
    x
  for ( ; i < n_samples; ++i ) {
    x = i * 2 / n_samples - 1
    curve[i] = ( 3 + k ) * x * 20 * deg / ( Math.PI + k * Math.abs(x) )
  } 
  return curve
}

const getters = {

}

export default {
  namespaced,
	state,
	mutations,
	actions,
	getters
}
