
const namespaced = true

const state = {

	init: false,

}

const mutations = {

	INIT (state, init) {

		state.init = init

	},

}

const actions = {

	init ({ commit, dispatch }) {

		commit('INIT', true)

	},

}

const getters = {

}

export default {
  namespaced,
	state,
	mutations,
	actions,
	getters
}
