
const namespaced = true

const state = {

  init: false,
  
  sequencers: [
    {
      name: 'sequencer-1',
      pattern: {
        name: 'bassline-1',
        steps: [],
      },
      waveform: 'square',
      isVisible: true,
      isMuted: false,
      volume: 0.8,
      octave: 0,
      delayLevel: 0.5,
      reverbLevel: 0.0,
    },
    {
      name: 'sequencer-2',
      pattern: {
        name: 'arpeggio-up-3',
        steps: [],
      },
      waveform: 'sawtooth',
      isVisible: true,
      isMuted: false,
      volume: 0.8,
      octave: 0,
      delayLevel: 0.0,
      reverbLevel: 1.0,
    },
    {
      name: 'sequencer-3',
      pattern: {
        name: 'default-empty',
        steps: [],
      },
      waveform: 'square',
      isVisible: false,
      isMuted: false,
      volume: 0.8,
      octave: 1,
      delayLevel: 0.5,
      reverbLevel: 0.0,
    },
    {
      name: 'sequencer-4',
      pattern: {
        name: 'default-empty',
        steps: [],
      },
      waveform: 'sawtooth',
      isVisible: false,
      isMuted: false,
      volume: 0.8,
      octave: 1,
      delayLevel: 0.0,
      reverbLevel: 1.0,
    },
  ]

}

const mutations = {

	INIT (state) {

		state.init = true

	},

}

const actions = {

	async init ({ commit, dispatch }) {

    await dispatch('loadPatterns')
    commit('INIT', true)

  },
  
  async loadPatterns ({ commit, dispatch }) {

    for (let sequencer of state.sequencers) {
      sequencer.pattern = await dispatch('Patterns/loadPatternByName', sequencer.pattern.name, { root: true })
    }

  },

}

const getters = {

  sequencers: (state) => state.sequencers

}

export default {
  namespaced,
	state,
	mutations,
	actions,
	getters
}
