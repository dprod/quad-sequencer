
import waaclock from 'waaclock'

const namespaced = true

const state = {

  init: false,

  clock: null,

  tempo: 140,

  stepDuration: 0,
  
  isPlaying: false,

  position: {
      step: 0,
      beat: 1,
      bar: 1,
  },

}

const mutations = {

	INIT (state) {
             
		state.init = true

  },

  CREATE_CLOCK (state, clock) {

    state.clock = clock

  },
  
  START_CLOCK (state) {

    state.isPlaying = true

  },
  
  STOP_CLOCK (state) {

    state.clock.stop()

    state.isPlaying = false

    state.position.step = 0
    state.position.beat = 1
    state.position.bar = 1

  },

  SET_TEMPO (state, tempo) {

    state.tempo = tempo

  },

  UPDATE_POSITION (state, position) {

    state.position = position

  }

}

const actions = {

	init ({ commit, dispatch }) {
      
    dispatch('createClock')
		commit('INIT')

  },

  createClock ({ commit, dispatch }) {

    const clock = new waaclock(this.getters['Master/context'], { toleranceEarly: 0.1 })
    commit('CREATE_CLOCK', clock)        

  },

	async startClock ({ commit, dispatch }) {

    await dispatch('stopClock')   

    // if(state.isPlaying)
    //   return

		commit('START_CLOCK')

    state.stepDuration = (60 / state.tempo) / 2       

    state.clock.start()

    state.clock
      .callbackAtTime(() => {
          dispatch('clockHandler')
      }, this.getters['Master/context'].currentTime)
			.repeat(state.stepDuration)
      .tolerance({ early: 0.01, late: 0.5 })
      
      console.log(`Clock :: startClock ${state.isPlaying}`)

  },

	stopClock ({ commit, dispatch }) {

    commit('STOP_CLOCK')
    
  },

  clockHandler ({ commit, dispatch }) {

    console.log(`Clock :: clockHandler`)
    dispatch('positionHandler')

  },

  positionHandler ({ commit, dispatch }) {

    // #TODO

    const position = {
      step: 0,
      beat: 0,
      bar: 0,
    }

    state.position.step = (state.position.step >= 16) ? 1 : state.position.step + 1

    state.position.beat = (state.position.step % 4 == 1) ? state.position.beat + 1 : state.position.beat
    state.position.beat = (state.position.beat > 4) ? 1 : state.position.beat

    state.position.bar = (state.position.step == 1) ? state.position.bar + 1 : state.position.bar
    state.position.bar = (state.position.bar > 4) ? 1 : state.position.bar
      
  },

  async setTempo ({ commit, dispatch }, tempo) {

    await dispatch('stopClock')
    commit('SET_TEMPO', tempo)
    dispatch('startClock')

  },

}

const getters = {

  isPlaying: state => state.isPlaying,
  
  position: state => state.position,

  tempo: state => state.tempo,

}

export default {
  namespaced,
	state,
	mutations,
	actions,
	getters
}
