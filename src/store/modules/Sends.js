
import axios from 'axios'

const namespaced = true

const state = {

	init: false,

	isVisible: false,

	sendEffects: [],

}

const mutations = {

	INIT (state, init) {

		state.init = init

	},

	SHOW (state) {

		state.isVisible = true

	},

	HIDE (state) {

		state.isVisible = false

	},

	ADD_EFFECT (state, effect) {

		state.sendEffects.push(effect)

	},

}

const actions = {

	async init ({ commit, dispatch }) {

        // Shortcut for context
		const context = this.getters['Master/context']	

        // ================================
        // DELAY
        // ================================

		let delay = {
			name: 'delay',
			effect: null,
			feedback: null,
			output: null,
			filter: null,
		}

		// Components
		delay.effect = context.createDelay()
		delay.feedback = context.createGain()
		delay.output = context.createGain()
		delay.filter = context.createBiquadFilter()

		// Settings
		delay.effect.delayTime.setValueAtTime(0.33, context.currentTime)
		delay.feedback.gain.setValueAtTime(0.5, context.currentTime)
		delay.output.gain.setValueAtTime(1 , context.currentTime)
		delay.filter.frequency.setValueAtTime(500, context.currentTime)
		delay.filter.type = 'highpass';

		// Connections
		delay.effect.connect(delay.feedback)
		delay.feedback.connect(delay.effect)
		delay.effect.connect(delay.filter)
		delay.filter.connect(delay.output)
		delay.output.connect(this.getters['Master/masterGain'])

		// Add effect to sends
		commit('ADD_EFFECT', delay)

        // ================================
        // REVERB (CONVOLUTION)
		// ================================
		
		let reverb = {
			name: 'reverb',
			effect: null,
			highpass: null,
			output: null,
		}
		
		// Components
		reverb.effect = context.createConvolver()
		reverb.effect.buffer = await dispatch('fetchImpulseFile')
		reverb.highpass = context.createBiquadFilter()
		reverb.output = context.createGain()

		// Settings
		reverb.highpass.type = 'highpass';
		reverb.highpass.frequency.setValueAtTime(1200, context.currentTime)
		reverb.highpass.Q.setValueAtTime(2.0, context.currentTime)
		reverb.output.gain.setValueAtTime(1.0, context.currentTime)

		// Connections
		reverb.effect.connect(reverb.highpass)
		reverb.highpass.connect(reverb.output)
		reverb.output.connect(this.getters['Master/masterGain'])

		// Add effect to sends		
		commit('ADD_EFFECT', reverb)

        // ================================
        // DISTORTION
        // ================================

		// let distortion = {
		// 	name: 'distortion',
		// 	effect: null,
		// }

        // distortion.effect = context.createWaveShaper()
        // distortion.effect.curve = makeDistortionCurve(50)
		// distortion.effect.oversample = '4x'
		
		// distortion.effect.connect(this.getters['Master/masterGain'])

		// // Add effect to sends
		// commit('ADD_EFFECT', distortion)

		// Init when ready

		commit('INIT', true)
		

	},

	async fetchImpulseFile ({ commit, dispatch }, reverb) {

		// #TODO

		let fileUri = `static/impulse4.wav`

		// if(process.env.NODE_ENV === 'development') {
		// 	console.log(``)
		// }

		// console.log(`fileUri ${fileUri}`)

		// if(process.env.NODE_ENV === 'production')
		// 	fileUri = `${process.env.NODE_ENV.APP_URI}/static/impulse4.wav`
		// else
		// 	fileUri = '/static/impulse4.wav'

		let data = await axios
			.request({
				url: fileUri,
				responseType: 'arraybuffer',
				method: 'get',
			})
			.then((response) => {
				Promise.resolve(response.data)
				return response.data
			})
			.catch((error) => Promise.reject(error))

		let impulseBuffer = await this.getters['Master/context'].decodeAudioData(data, (buffer) => {
			return buffer
		})

		return impulseBuffer

	},

	show ({ commit, dispatch }) {

		commit('SHOW')

	},

	hide ({ commit, dispatch }) {

		commit('HIDE')

	},



}

function makeDistortionCurve(amount) {
	var k = typeof amount === 'number' ? amount : 50,
	  n_samples = 44100,
	  curve = new Float32Array(n_samples),
	  deg = Math.PI / 180,
	  i = 0,
	  x
	for ( ; i < n_samples; ++i ) {
	  x = i * 2 / n_samples - 1
	  curve[i] = ( 3 + k ) * x * 20 * deg / ( Math.PI + k * Math.abs(x) )
	} 
	return curve
  }
  

const getters = {

	getSendEffectByName(state) {
		return effectName => state.sendEffects.find((effect) => effectName.toLowerCase() === effect.name.toLowerCase())
	},

	sendEffects: (state) => state.sendEffects,

	isVisible: (state) => state.isVisible,

}

export default {
	namespaced,
	state,
	mutations,
	actions,
	getters
}
