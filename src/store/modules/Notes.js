
const namespaced = true

const state = {

    init: false,
    
    settings: {
        middleAnoteNumber: 59,
        middleCnoteNumber: 62,
        tuning: 440,
        bottomNoteNumber: 12,
        topNoteNumber: 120,
        noteNames: [
            'C-',
            'C#',
            'D-',
            'D#',
            'E-',
            'F-',
            'F#',
            'G-',
            'G#',
            'A-',
            'A#',
            'B-',
        ]    
    },

    notes: [],

}

const mutations = {

	INIT (state) {

		state.init = true

    },
    
    CREATE_NOTES (state, notes) {

        state.notes = notes

    },

}

const actions = {

	init ({ commit, dispatch }) {

        dispatch('createNotes')
        commit('INIT')
        
	},

	createNotes ({ commit, dispatch }) {

        let notes = []       
        
		for (var noteNumber = state.settings.bottomNoteNumber; noteNumber <= state.settings.topNoteNumber; noteNumber++) {

			const note = {
				number: noteNumber,
				name: getNoteNameByNoteNumber(noteNumber) + getOctaveByNoteNumber(noteNumber),
				frequency: getFrequencyByNoteNumber(noteNumber),
			}

			notes.push(note)

        }

		commit('CREATE_NOTES', notes)

    },
    
}

const getters = {

    notes: (state) => {

        return state.notes

    },

    getNoteByNoteNumber(state) {
        return noteNumber => state.notes.find((note) => note.number === noteNumber)
    }


}

// Note creation helpers

function getOctaveByNoteNumber(noteNumber) {
    return Math.floor((noteNumber / 12) - 1)
}

function getFrequencyByNoteNumber(noteNumber) {
    return state.settings.tuning * Math.pow(2, (noteNumber - state.settings.middleAnoteNumber) / 12)
}

function getNoteNameByNoteNumber(noteNumber) {
    return state.settings.noteNames[noteNumber % 12]
}

export default {
    namespaced,
	state,
	mutations,
	actions,
	getters
}
