
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import modules from './modules'

const store = new Vuex.Store({
  modules,
  actions: {},
	getters: {
    route: state => state.route,
    ready: (state) => {

      // App is ready when modules are ready and initialized.
      if ( state.Master.init && state.Clock.init && state.Sends.init && state.Sequencers.init )
        return true
      else
        return false

    },
 	}
})

store.subscribe((mutation, state) => {

	if(mutation.type === 'route/ROUTE_CHANGED') {
		console.log(`%cRoute changed from %c${mutation.payload.from.path} %cto %c${mutation.payload.to.path}`, `font-weight: normal;`, `font-weight: bold;`, `font-weight: normal;`, `font-weight: bold;`)
	}

})

export default store
